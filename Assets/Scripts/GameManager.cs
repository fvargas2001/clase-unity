using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{

    [SerializeField] private GameObject gameOverScreen;
    [SerializeField] private TMP_Text scoreText;
    [SerializeField] private float initialScrollSpeed;

    private int score;
    private float timer;
    private float scrollSpeed;

    private int puntos;
    [SerializeField] private TMP_Text puntosText;

    public AudioSource backgroundMusic;

    // Start is called before the first frame update
    public static GameManager Instance { get; private set; }
    
    private void Awake()
    {
        if(Instance !=null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    public void PlayerLost()
    {
        backgroundMusic.Stop();
    }

        // Update is called once per frame
        void Update()
    {
        UpdateScore();
        UpdateSpeed();
    }

    public void ShowGameOverScreen()
    {
        gameOverScreen.SetActive(true);
    }

    public void RestarScreen()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1f;
    }

    private void UpdateScore()
    {
        int scorePerSecond = 10;

        timer += Time.deltaTime;
        score = (int)(timer * scorePerSecond);
        scoreText.text = string.Format("{0:000000}", score);
    }

    public float GetScrollSpeed()
    {
        return scrollSpeed;
    }

    private void UpdateSpeed()
    {
        float speedDivider = 10f;
        scrollSpeed = initialScrollSpeed + timer / speedDivider;
    }

    public void addPuntos()
    {
        puntos++;
        puntosText.text = "Score = " + puntos.ToString();
    }
}
