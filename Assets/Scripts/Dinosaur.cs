using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dinosaur : MonoBehaviour
{
    [SerializeField] private float upForce;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private LayerMask ground;
    [SerializeField] private float radius;

    private Rigidbody2D dinoRb;
    private Animator dinoAnimator;
    private AudioSource SonidoDeSalto;

    public GameObject Bullet;

    // Start is called before the first frame update
    void Start()
    {
        dinoRb = GetComponent<Rigidbody2D>();
        dinoAnimator = GetComponent<Animator>();
        SonidoDeSalto = GetComponent<AudioSource>();
        
    }
    
    // Update is called once per frame
    void Update()
    {
        bool isGrounded = Physics2D.OverlapCircle(groundCheck.position, radius, ground);
        dinoAnimator.SetBool("IsGrounded", isGrounded);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (isGrounded)
            {
                dinoRb.AddForce(Vector2.up * upForce);
                SonidoDeSalto.Play();
            }
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            Instantiate(Bullet, transform.position, Quaternion.identity);

        }

    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(groundCheck.position, radius);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Obstacle"))
        {
             GameManager.Instance.ShowGameOverScreen();
             dinoAnimator.SetTrigger("Die");
             Time.timeScale = 0f;
             GameManager.Instance.PlayerLost();
        }
        else if (collision.gameObject.CompareTag("Enemy"))
        {
            GameManager.Instance.ShowGameOverScreen();
            dinoAnimator.SetTrigger("Die");
            Time.timeScale = 0f;
            GameManager.Instance.PlayerLost();
        }
        else if (collision.gameObject.CompareTag("Moneda"))
        {
            Destroy(collision.gameObject);
            GameManager.Instance.addPuntos();
        }
    }
}
